// Modules
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { UserModule } from './modules/user/user.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

// Components
import { AppComponent } from './app.component';

// Directives
import { LinkDirective } from './directives/link.directive';
import { CounterComponent } from './components/counter/counter.component';
import { UserService } from './modules/user/services/user.service';
import { ErrorService } from './services/error.service';
import { ComplexDepsService } from './services/complex-deps.service';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { ForbiddenDirective } from './directives/forbidden.directive';
import { FormMultistepComponent } from './components/form-multistep/form-multistep.component';
import { AnchorDirective } from './directives/anchor.directive';

@NgModule({
  declarations: [
    AppComponent,
    LinkDirective,
    CounterComponent,
    LoginComponent,
    ForbiddenDirective,
    FormMultistepComponent,
    AnchorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    /**
     * Quando un modulo è declinato all'interno dell'array imports di un altro modulo,
     * viene sfruttato il caricamento Eager Loading, attraverso il quale l'istanza
     * del modulo viene creata nello stesso momento in cui viene avviata l'applicazione
     * nel browser
     */
    HttpClientModule,
    UserModule
  ],
  providers: [
    // {
      // provide: UserService,
      // useClass: UserService
    // }
    {
      provide: ErrorHandler,
      useExisting: ErrorService
    }
    // Esempio di Factory Pattern in Angular (factory function con dipendenze da DI)
    //, {
    //   deps: [HttpClient],
    //   provide: ComplexDepsService,
    //   useFactory: (http: HttpClient) => new ComplexDepsService(http, 'ciaone')
    // }
  ],
  bootstrap: [AppComponent],
  // entryComponents: [FormStepOneComponent] -< DEPRECATO CON IVY RENDER
})
export class AppModule { }
