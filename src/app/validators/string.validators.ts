import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms"

export const forbiddenValidator = (...values: string[]): ValidatorFn => {
    return (c: AbstractControl): ValidationErrors | null => {
        if (values.includes(c.value)) {
            return {
                forbidden: true
            };
        }

        return null;
    }
}
