import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMultistepComponent } from './form-multistep.component';

describe('FormMultistepComponent', () => {
  let component: FormMultistepComponent;
  let fixture: ComponentFixture<FormMultistepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormMultistepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormMultistepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
