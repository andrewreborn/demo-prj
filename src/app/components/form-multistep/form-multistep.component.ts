import { AfterViewInit, Component, ComponentFactoryResolver, OnInit, Type, ViewChild } from '@angular/core';
import { AnchorDirective } from 'src/app/directives/anchor.directive';
import { FormStepOneComponent } from '../form-step-one/form-step-one.component';

@Component({
  selector: 'app-form-multistep',
  templateUrl: './form-multistep.component.html',
  styleUrls: ['./form-multistep.component.scss']
})
export class FormMultistepComponent implements AfterViewInit, OnInit {

  // @ViewChild(AnchorDirective) public anchor!: AnchorDirective;

  public stepComponent!: Promise<Type<FormStepOneComponent>>;

  constructor(
    // Serve a restituire una Component Factory a partire dalla definizione di un Componente
    // private _cfr: ComponentFactoryResolver
  ) { }

  public ngAfterViewInit(): void {
    // Metodo da usare per le Component Factory PRIMA di Ivy Render
    // const f = this._cfr.resolveComponentFactory(FormStepOneComponent);
    // this.anchor.vcr.createComponent(f);
  }

  ngOnInit(): void {
    this.stepComponent = import('../form-step-one/form-step-one.component').then(c => c.FormStepOneComponent);
  }

}
