import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  @Input() public cnt: number = 0;
  @Output() public limitReached: EventEmitter<number> = new EventEmitter();

  constructor() { }

  public handleCounter(isSum: boolean): void {
    isSum ? this.cnt++ : this.cnt--;

    if (this.cnt > 10) {
      this.limitReached.emit(this.cnt);
    }
  }

  ngOnInit(): void {
  }

}
