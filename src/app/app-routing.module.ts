import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormMultistepComponent } from './components/form-multistep/form-multistep.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [{
  component: LoginComponent,
  path: 'login'
}, {
  component: FormMultistepComponent,
  path: 'multistep'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
