import { HttpClient } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService extends ErrorHandler {

  constructor(private _http: HttpClient) {
    super();
  }

  public handleError(error: Error): void {
    super.handleError(error);

    // this._http.post('') ...
  }
}
