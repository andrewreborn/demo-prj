import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComplexDepsService {

  constructor(private _http: HttpClient) { }
}
