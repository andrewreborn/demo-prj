import { TestBed } from '@angular/core/testing';

import { ComplexDepsService } from './complex-deps.service';

describe('ComplexDepsService', () => {
  let service: ComplexDepsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComplexDepsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
