import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { forbiddenValidator } from '../validators/string.validators';

@Directive({
  selector: '[appForbidden]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ForbiddenDirective,
    multi: true
  }]
})
export class ForbiddenDirective implements Validator {

  @Input('appForbidden') public values!: string[];

  constructor() { }

  public validate(c: AbstractControl) {
    // const fn = forbiddenValidator('admin@dominio.com', 'moderator@dominio.com');
    // return fn(c);

    return forbiddenValidator(...this.values)(c);
  }

}
