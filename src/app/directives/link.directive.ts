import { Directive, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[appLink]'
})
export class LinkDirective {

  @Input('appLink') linkSegments!: string[];

  constructor(private _router: Router) { }

  @HostListener('click')
  public navigate(): boolean {
    // Chiamare il router e fare una navigazione al percorso indicato
    this._router.navigate(this.linkSegments);
    return false;
  }

}
