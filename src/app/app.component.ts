import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public start = 5;
  public title = 'demo-prj';

  public handleLimitReached(): void {
    alert('Limite raggiunto!!');
  }
}
