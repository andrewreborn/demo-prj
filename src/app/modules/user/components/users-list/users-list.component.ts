import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users-list', // crea un simil-tag HTML <app-users-list>
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  providers: [UserService]
})
export class UsersListComponent implements OnInit {

  public users: User[] = [];
  
  constructor(private _userService: UserService) { }

  ngOnInit(): void {
    this._userService.list().subscribe(users => this.users = users);
  }

}
