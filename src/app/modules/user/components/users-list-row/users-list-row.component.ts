import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: '[app-users-list-row]', // <selettore app-users-list-row></selettore>
  templateUrl: './users-list-row.component.html',
  styleUrls: ['./users-list-row.component.scss'],
  providers: []
})
export class UsersListRowComponent implements OnInit {

  @Input() user!: User;
  
  constructor(private _userService: UserService) { }

  ngOnInit(): void {
  }

}
