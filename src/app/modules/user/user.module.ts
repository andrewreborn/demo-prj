import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersListRowComponent } from './components/users-list-row/users-list-row.component';


@NgModule({
  declarations: [
    UsersListComponent,
    UsersListRowComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule {
  constructor() {
    console.log('USER MODULE');
  }
}
