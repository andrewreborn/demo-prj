// export class User {
//     public email: string;
//     public readonly id: number;
//     public name: string;

//     constructor(id: number, email: string, name: string) {
//         this.email = email;
//         this.id = id;
//         this.name = name;
//     }
// }

export class User {
    constructor(
        public id: number,
        public email: string,
        public name: string
    ) {
        //
    }
}
