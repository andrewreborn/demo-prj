import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { map } from 'rxjs/operators';
import { IUser } from '../interfaces/user.interface';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { ComplexDepsService } from 'src/app/services/complex-deps.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _objToModel(user: IUser): User {
    return new User(user.id, user.email, user.name);
  }

  private _objsToModels(users: IUser[]): User[] {
    return users.map(user => this._objToModel(user));
  }

  constructor(private _http: HttpClient) {
    console.log('USER SERVICE CONSTRUCTOR!!');
  }

  public list(): Observable<User[]> {
    return this._http.get<IUser[]>(`${environment.api.url}/users`).pipe(
      map(users => this._objsToModels(users))
    );
  }
}
