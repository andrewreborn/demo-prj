import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';

const routes: Routes = [{
  children: [{
    component: UsersListComponent,
    path: '' // /users
    // }, {
    //   component: 
    //   path: ':id' // /users/123
    //
  }, {
    loadChildren: () => import('../todo/todo.module').then(m => m.TodoModule),
    path: ':userId/todos'
  }],
  path: 'users'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
