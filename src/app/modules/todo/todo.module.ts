import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { TodosListComponent } from './components/todos-list/todos-list.component';


@NgModule({
  declarations: [TodosListComponent],
  imports: [
    CommonModule,
    TodoRoutingModule
  ]
})
export class TodoModule {
  constructor() {
    console.log('Todos Module');
  }
}
