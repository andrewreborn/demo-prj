import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent implements OnInit {

  constructor(private _route: ActivatedRouteSnapshot) { }

  ngOnInit(): void {
    const id = this._route.paramMap.get('userId');
  }

}
